### the-nick-wilson.com
Welcome to the repo behind my own website: the-nick-wilson.com!

Contact me at:
* [LinkedIn](https://www.linkedin.com/in/nicholaswilson05/)
* [GitLab](https://gitlab.com/the-nick-wilson)
* [GitHub](https://github.com/the-nick-wilson)
* [SO](https://stackoverflow.com/users/1305718/the-nick-wilson)
* [Twitter](https://twitter.com/the__nickwilson)
