
const scrollDivisor = 1;
const indexMultiplier = 20;
let scrollIndex = 0;
let frames = 0;
let minHeight = 0;
let windowHeight = 0;

const recalculateMinHeight = () => {
    windowHeight = $(window).height();
    const containerTop = $('#container')
        .css('top')
        .replace('px', '');
    minHeight = windowHeight + frames * indexMultiplier + 100;
    $('#height-buffer').css('height', minHeight);
    $('#container').css('height', windowHeight - containerTop - 23); // 18 = body margin (8) + container padding (10) + extra padding (5)
    scrollDivDown();
};

$(document).ready(function() {
    const inputTexts = $('.input-text');
    inputTexts.each((i, elem) => {
        let text = $(elem)
            .text()
            .trim()
            .split('');
        frames += text.length;
        $(elem).empty();
        text.map((char, i) => {
            $(elem).append(`<span class="off animated-text">${char}</span>`);
        });
        $('body').css('display', 'block');
    });

    const outputTexts = $('.output-text');
    frames += outputTexts.length;
    outputTexts.each((i, elem) => {
        $(elem)
            .addClass('off')
            .addClass('animated-text');
    });

    recalculateMinHeight();
});

$(window).scroll(function() {
    scrollIndex = $(window).scrollTop();
    scrollIndexDivided = Math.floor(scrollIndex / scrollDivisor);

    $('.animated-text').each((i, element) => {
        const multipliedIndex = i * indexMultiplier;

        if (multipliedIndex < scrollIndexDivided) {
            $(element)
                .removeClass('off')
                .addClass('on');
        } else {
            $(element)
                .removeClass('on')
                .addClass('off');
        }
    });
    scrollDivDown();
});

$(window).resize(() => {
    recalculateMinHeight();
});

const scrollDivDown = () => {
    const container = document.getElementById('container');
    container.scrollTop = container.scrollHeight;
};
